import pandas as pd
import matplotlib.pyplot as plt

def plot_histogram_duration(dataframe):
    dataframe['minutes'] = pd.to_numeric(dataframe['minutes'], errors='coerce')
    plt.figure(figsize=(12, 6))
    plt.hist(dataframe['minutes'].dropna(), bins=60, range=(0, 300), color='skyblue', edgecolor='black')
    plt.title('Distribuição da Duração das Receitas')
    plt.xlabel('Tempo (minutos)')
    plt.ylabel('Número de Receitas')
    plt.savefig('histograma_duracao.png')
    plt.show()

def plot_boxplot_steps(dataframe):
    plt.figure(figsize=(10, 6))
    plt.boxplot(dataframe['n_steps'], vert=False)
    plt.title('Distribuição do Número de Passos de Preparo')
    plt.xlabel('Número de Passos')
    plt.savefig('boxplot_passos.png')
    plt.show()

def plot_histogram_calories(dataframe):
    dataframe['calories'] = dataframe['nutrition'].apply(lambda x: eval(x)[0] if isinstance(x, str) and eval(x) else None)
    dataframe = dataframe.dropna(subset=['calories'])
    plt.figure(figsize=(12, 6))
    plt.hist(dataframe['calories'], bins=100, range=(0, 3000), color='lightgreen', edgecolor='black')
    plt.title('Distribuição de Calorias nas Receitas')
    plt.xlabel('Calorias')
    plt.ylabel('Número de Receitas')
    plt.savefig('histograma_calorias.png')
    plt.show()

def plot_histogram_ingredients(dataframe):
    plt.figure(figsize=(10, 6))
    plt.hist(dataframe['n_ingredients'], bins= 20, color='lightcoral', edgecolor='black')
    plt.title('Distribuição do Número de Ingredientes nas Receitas')
    plt.xlabel('Número de Ingredientes')
    plt.ylabel('Número de Receitas')
    plt.savefig('histograma_ingredientes.png')
    plt.show()

# Ler o CSV
df = pd.read_csv("./datasets/food-recipes/RAW_recipes.csv")

# Chamar as funções de plotagem
plot_histogram_duration(df)
plot_boxplot_steps(df)
# plot_histogram_calories(df)
plot_histogram_ingredients(df)


