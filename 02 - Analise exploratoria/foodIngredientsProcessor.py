import pandas as pd

# Ler o CSV
df = pd.read_csv("./datasets/food-restrictions/food_ingredients_and_allergens.csv")

# Manter apenas as colunas desejadas
df = df[["Food Product", "Allergens"]]

# Remover duplicatas, se necessário
df = df.drop_duplicates()

# Remover linhas sem informações em "Allergens"
df = df.dropna(subset=["Allergens"])

# Filtrar registros que possuem "Dairy" ou "Wheat" em "Allergens"
df = df[df["Allergens"].str.contains("Dairy|Wheat")]

# Remover outros alérgenos que não sejam "Dairy" ou "Wheat"
df["Allergens"] = df["Allergens"].apply(lambda x: ", ".join([a.strip() for a in x.split(',') if a.strip() in ["Dairy", "Wheat"]]))

# Remover linhas sem informações em "Allergens" após a filtragem
df = df[df["Allergens"] != ""]

# Salvar o novo CSV
df.to_csv("./datasets/food-restrictions/processed.csv", index=False)
